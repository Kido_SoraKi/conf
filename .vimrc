set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

Plugin 'rainbow_parentheses.vim'
Plugin 'syntastic'
Plugin 'ctrlp.vim'
Plugin 'beyondmarc/glsl.vim'
call vundle#end()

let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_no_default_include_dirs = 1
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = '-std=c++11'
let g:glsl_file_extensions = '*.glsl,*.vsh,*.fsh,*.vert,*.frag'

filetype plugin indent on

set encoding=utf-8
set fileencoding=utf-8
set noswapfile
set showmode
set showcmd
set relativenumber
set number
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
" always show status line
set laststatus=2
set statusline=%f\ %m%=%y\ %p%%\ %l:%c
" Enhence command line completion
set wildmenu

set tabpagemax=99

set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set autoindent
set hlsearch

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" from the Vim wiki
" http://vim.wikia.com/wiki/
" Restore_cursor_to_file_position_in_previous_editing_session
function! ResCur()
    if line("'\"") <= line("$")
        normal! g`"
        return 1
    endif
endfunction

augroup resCur
    autocmd!
    autocmd BufWinEnter * call ResCur()
augroup END

" Persistent undo
if version >= 703
    set undofile
    set undodir=~/.vimtmp/undo
    silent !mkdir -p ~/.vimtmp/undo
endif
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call pathogen#infect()

syntax on
set showmatch
set wrap
set textwidth=79
set cursorline
set colorcolumn=+1
set scrolloff=6

cmap w!! w !sudo tee >/dev/null %

set backspace=2
let g:jellybeans_background_color_256='232'
colorscheme jellybeans2
nmap    <ESC>[5^    <C-PageDown>
nmap    <ESC>[6^    <C-PageUp>
nnoremap <C-t> :execute "tabedit %:p:h"<CR>

function! MakeClass(arg)
    exec "tabedit %:p:h".'/'.a:arg.'.hh'
    let garde=toupper(a:arg).'_HH'
    let name=substitute(a:arg, '\(\w\)\(\w*\)', '\u\1\2', 'g')
    call setline('.', [ '#ifndef '.garde, '# define '.garde, '', 'class '.name, '{', '    public:', '    private:', '};', '', '#endif /* !'.garde.' */' ])
    exec "vs %:p:h".'/'.a:arg.'.cc'
    call setline('.', [ '#include "'.a:arg.'.hh"' ])
endfunction

function! Stos()
    set noet
    set tabstop=8
    set softtabstop=8
    set shiftwidth=8
endfunction

filetype plugin indent on
