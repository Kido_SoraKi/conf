#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
import os
from os.path import expanduser
import time

def inter_color(p, c1, c2):
  r1, g1, b1 = int(c1[1:3], 16), int(c1[3:5], 16), int(c1[5:7], 16)
  r2, g2, b2 = int(c2[1:3], 16), int(c2[3:5], 16), int(c2[5:7], 16)
  dr, dg, db = r2 - r1, g2 - g1, b2 - b1
  r, g, b = r1 + p * dr, g1 + p * dg, b1 + p * db
  return '#%02X%02X%02X' % (int(r), int(g), int(b))

def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())
    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','
        j = json.loads(line)
        # insert information into the start of the json, but could be anywhere
        #min_val = +inf, mean_val = 0, nb = 0, max_val = -inf, cpu_temp_index = -1
        #index = 0
        for e in j:
            if e["name"] == "cpu_usage":
                percent = int(''.join(c for c in e["full_text"] if c.isdigit())) / 100.
                e["color"] = inter_color(percent, "#4DBD33", "#FF0033")
            elif e["name"] == "cpu_temperature":
        #        if cpu_temp_index < 0:
        #            cpu_temp_index = index
                value = int(''.join(c for c in e["full_text"] if c.isdigit()))
        #        min_val = (min(value, min_val), e);
        #        max_val = (max(value, max_val), e);
        #        mean_val = mean_val + value;
        #        nb = nb + 1
                percent = min(60, max(0, value - 20))
                e["color"] = "#E0218A" if (percent == 22) else inter_color(percent / 60., "#4DBD33", "#FF0033")
        #    index = index + 1
        #min_val[1]["name"] = "min_cpu_temperature"
        #min_val[1]["full_text"] = '↓' + max_val[1]["full_text"]
        #mean_val = mean_val / nb
        #max_val[1]["name"] = "max_cpu_temperature"
        #max_val[1]["full_text"] = '↑' + max_val[1]["full_text"]
        #for e in j:
        #    if e["name"] == "cpu_temperature":
        #        j.remove(e)
        #j.insert(cpu_temp_index, max_val[1])
        #j.insert(cpu_temp_index, {"name":"mean_cpu_temperature","instance":"","full_text":"44°C"})
        #j.insert(cpu_temp_index, min_val[1])
        # and echo back new encoded json
        print_line(prefix+json.dumps(j))
