# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' rehash true
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-/]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 3 numeric
zstyle ':completion:*' menu select=1
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/root/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# by néco

export TERM=rxvt-unicode-256color
export CC=clang
export CXX=clang++
export PAGER="less"
export NNTPSERVER='news.epita.fr'
export editor="vim"
export EDITOR="vim"
export PATH=$PATH:/usr/local/gcc-arm-none-eabi-4_9-2014q4/bin

# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

setopt appendhistory extended_glob HIST_IGNORE_ALL_DUPS HIST_SAVE_NO_DUPS sh_word_split
unsetopt beep notify

export CLICOLOR="YES"
export LSCOLORS="ExGxFxdxCxDxDxhbadExEx"

alias reboot='systemctl reboot'
alias poweroff='systemctl poweroff'
alias z='i3lock -c 000000 -i ~/bg.jpg'
alias suspend='z; systemctl suspend'
alias cd.='cd ..'
alias cd-='cd -'
alias ls='ls --color=auto'
alias ll='ls -l'
alias l='ll'
alias la='ls -la'
alias lla='ls -la'
alias reload="source ~/.zshrc"
alias c='clear'
alias rsn='redshift -O 5000'
alias rsd='redshift -O 6500'
alias authors='echo "* allain_n" > AUTHORS; cat -e AUTHORS'
alias ragequit='poweroff'
alias serv='mosh kido@unic0rn.eu'
alias stos-debug='qemu-system-x86_64 -nographic -monitor pty -serial stdio -serial pty  stos-i386-pc.boot'
alias xleft='xrandr --output VGA-0 --mode 1280x1024 --left-of LVDS-0'
alias xright='xrandr --output VGA-0 --mode 1280x1024 --right-of LVDS-0'
alias gti='git'

autoload -U colors && colors

PROMPT="%(!.%F{red}%B.%F{white})%n %F{cyan}%~%f%#%f%b "
RPROMPT='%F{blue}%T%f%f'
setopt nopromptcr

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

bindkey -e
bindkey '^W' vi-backward-kill-word
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line
bindkey "^[[3~" delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey '[D' emacs-backward-word
bindkey '[C' emacs-forward-word
